#-*- coding: utf-8 -*-
import pandas as pd 

df = pd.read_excel("cisNAVERdata_1128.xlsx", index_col=u"날짜")
print df.index 		# show dataframe index
print df.iloc[1]	# int index 
print df.loc['2017.11.04':'2017.11.09']	# from 11.04 ~ 11.09
# print df.groupby(pd.TimeGrouper('2D')).sum()	# this cause error because of index type

df.index = pd.to_datetime(df.index)
print df.groupby(pd.TimeGrouper('2D')).sum()	# this cause error because of index type
target_date = pd.to_datetime('20171101')
print df.loc[target_date]

######## important to remember
# df.index = pd.to_datetime(df.index)
# print df.index
# print df

#-*- coding: utf-8 -*-
# u"안녕"

import pandas as pd 
# import sys
# reload(sys)
# sys.setdefaultencoding('utf-8')
df = pd.read_excel("data20170611.xlsx", index_col="Date", encoding='utf8')

df.index = pd.to_datetime(df.index)
start = pd.Timestamp('2017-06-01')
end = pd.Timestamp('2017-07-01')

# df.index => Date, because index_col="Date"
# condition is df.index < end(2017-07-01)
# get column only "name", if want to get all columns => :
# value_counts => count frequency
print df.ix[df.index < end, :]
name_count_df = df.ix[df.index < end,"name"].value_counts()
name_count_df.to_csv("value_count.csv", encoding='utf-8-sig')
# print df.index.values[0].decode("utf-8")

# ["권희선","하다울", "송미정","이은숙","배보람","주연숙","정연보","노묘진","노묘진"]

# set1 = set(name_list)
# set("권희선","하다울", "송미정","이은숙","배보람","주연숙","정연보","노묘진")

# name_list = list(set1)
# print name_list
# ["권희선","하다울", "송미정","이은숙","배보람","주연숙","정연보","노묘진"]

# make list => set => list
# list = []
# set = set(list)
# list1 = [1,2,3,1]
# set1 = set(list1) 
# => set1 => set(1,2,3)
# list2 = list(set1)
# =>[1,2,3]

# the number of students
# print len(name_list)
#-*- coding: utf-8 -*-
import pandas as pd

class_list = ["CJC", u"회화", u"초급회화",u"통번역"]
time_list = ["6M","M", "Q"]

for time in time_list:
	for class_name in class_list:
		df = pd.read_excel("record2017.xlsx", index_col="register_date")
		# date => pandas's date
		df.index = pd.to_datetime(df.index)
		class_value_list = df.ix[df['class'].str.contains(class_name), "class"].values.tolist()
		class_value_list = set(class_value_list)
		class_value_list = list(class_value_list)
		print class_value_list
		name_df = pd.DataFrame(class_value_list)
		print name_df
		name_df.to_excel(class_name+".xlsx")
		g = df.groupby(pd.TimeGrouper(time))
		result_df = g.apply(lambda x: x['class'].str.contains(class_name).value_counts())
		result_df = result_df[True].to_frame()
		result_df.to_excel(class_name+"_by_"+time+".xlsx")



#-*- coding: utf-8 -*-
import pandas as pd 

df = pd.read_excel("record2017.xlsx", index_col="No")

# print df.ix[df['payment'] < 0, 'name']	

minus_payment = df.ix[df['payment'] < 0, 'name'].value_counts()
# print minus_payment

all_payment = df['name'].value_counts()
# print all_payment.index
all_payment.to_excel("original_payment.xlsx")
# print all_payment.ix[minus_payment.index==all_payment.index, :] => do not this!
sub_all_payment = all_payment.ix[minus_payment.index]
# print type(minus_payment)
# print type(sub_all_payment)
real_payment = sub_all_payment - minus_payment
all_payment.ix[real_payment.index] = real_payment
# all_payment = real_payment => do not this! need to set same index!!
all_payment.to_excel("result_payment.xlsx")


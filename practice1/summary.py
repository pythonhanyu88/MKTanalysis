#-*- coding: utf-8 -*-
import pandas as pd 

########## call input data
df = pd.read_excel("record2017.xlsx", index_col="No")

########## student counting
student_df = df["name"].value_counts()
student_df.to_csv("student_freq.csv", encoding='utf-8-sig')

########## total counting
class_freq = df["class"].value_counts()
# dataframe.to_csv("파일이름.csv", encoding='utf-8-sig')
class_freq.to_csv("class_freq.csv", encoding='utf-8-sig')

########## basic counting
# print df[df['class'].str.contains(u"입문")]
print df.ix[df['class'].str.contains(u"입문"), :]	# dataframe
basic = df.ix[df['class'].str.contains(u"입문"), :]

# basic["class"] => pandas Series
# basic["class"].value_counts() => Series
class_freq = basic["class"].value_counts() 
class_freq.to_csv("basic_class_freq.csv", encoding='utf-8-sig')

########## extract speaking from basic
# call index column => df.index
# call one column = df["column_name"]
print class_freq.index.str.contains(u"회화")
# print class_freq.ix[class_freq.index.str.contains(u"회화"), :] => do not this
# print class_freq.ix[class_freq.index.str.contains(u"회화")] => this is okay
spk = class_freq[class_freq.index.str.contains(u"회화")]
spk.to_csv("basic_speaking_class_freq.csv", encoding='utf-8-sig')


#-*- coding: utf-8 -*-
import pandas as pd 

df = pd.read_excel("record2017.xlsx", index_col="No")

minus_payment_class = df.ix[df['payment'] < 0, 'class'].value_counts()

all_payment_class = df['class'].value_counts()

all_payment_class.to_excel("original_payment_class.xlsx")

sub_all_payment_class = all_payment_class.ix[minus_payment_class.index]

real_payment_class = sub_all_payment_class - minus_payment_class
all_payment_class.ix[real_payment_class.index] = real_payment_class

all_payment_class.to_excel("result_payment_class.xlsx")
